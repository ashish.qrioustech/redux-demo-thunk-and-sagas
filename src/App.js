import React from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from "react-router-dom";
import ReduxSagas from "./reduxSagas";
import ReduxThunk from "./reduxThunk";

function App() {
  return (
    <div className="App">
      <Router basename="/redux-demo">
        <Switch>
          <Route path="/thunk" exact component={ReduxThunk} />
          <Route path="/sagas" component={ReduxSagas} />

          <Redirect to="/thunk" />
        </Switch>
      </Router>
      {/* <ReduxThunk /> */}

      {/* <ReduxSagas /> */}
    </div>
  );
}

export default App;
