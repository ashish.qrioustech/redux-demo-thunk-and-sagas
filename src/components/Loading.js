import React from "react";
import { connect } from "react-redux";
import img from "../images/Loading1.gif";
let Loading = ({ loading }) =>
  loading ? (
    <div style={{ textAlign: "center" }}>
      <img
        className="img-fluid"
        style={{ height: 200, width: 200, borderRadius: "50%" }}
        src={img}
        alt="loading"
      />
      <h1>LOADING</h1>
    </div>
  ) : null;
const mapStateToProps = (state) => ({ loading: state.news.loading });
Loading = connect(mapStateToProps, null)(Loading);
export default Loading;
