import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import {
  increaseCountAction,
  decreaseCountAction,
} from "./actions/counterAction";

const getCountSelector = (state) => {
  return state.counter.count;
};
const reduxThunk = ({ count, increaseCount, decreaseCount }) => {
  return (
    <>
      <h1 className="my-3 text-danger">Redux Thunk Demo</h1>
      <div className="d-flex">
        <Link to="/sagas" className="btn btn-outline-danger my-2 mx-auto">
          {"Go to the Sagas ⇒"}
        </Link>
      </div>
      <h5 className="my-3">Count = {count}</h5>
      <div>
        <button
          className="btn btn-outline-success mx-2"
          onClick={increaseCount}
        >
          Increment
        </button>
        <button className="btn btn-outline-danger" onClick={decreaseCount}>
          decrement
        </button>
      </div>
    </>
  );
};
const mapStateToProps = (state) => ({
  count: getCountSelector(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      increaseCount: increaseCountAction,
      decreaseCount: decreaseCountAction,
    },
    dispatch
  );
export default connect(mapStateToProps, mapDispatchToProps)(reduxThunk);
