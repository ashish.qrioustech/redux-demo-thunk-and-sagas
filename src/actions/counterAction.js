import { INCREASE_COUNTER, DECREMENT_COUNTER } from "../type";

export const increaseCount = () => ({
  type: INCREASE_COUNTER,
});

export const increaseCountAction = () => {
  return (dispatch) => dispatch(increaseCount());
};

export const decreaseCount = () => ({
  type: DECREMENT_COUNTER,
});

export const decreaseCountAction = () => {
  return (dispatch) => dispatch(decreaseCount());
};