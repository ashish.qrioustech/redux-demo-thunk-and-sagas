import { combineReducers } from "redux";
import counter from "./counterReducer";
import news from "./newsSagaReducer";

const appReducer = combineReducers({ counter, news });

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

export default rootReducer;
