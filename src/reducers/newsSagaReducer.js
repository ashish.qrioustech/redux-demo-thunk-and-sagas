import {
  GET_NEWS,
  NEWS_RECEIVED,
} from "../type";

const initialState = {};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NEWS:
      return { ...state, loading: true ,news:"" };
    case NEWS_RECEIVED:
      return { ...state, news: action.json[0], loading: false };
    default:
      return state;
  }
};
export default reducer;
