import React from "react";
import Button from "./components/Buttons";
import NewsItem from "./components/NewsItem";
import Loading from "./components/Loading";
import { Link } from "react-router-dom";

const reduxSagas = () => {
  return (
    <div>
      <h1 className="my-3 text-danger">Redux Sagas Demo</h1>
      <div className="d-flex">
        <Link to="/thunk" className="btn btn-outline-danger mx-auto mt-2">
          {"↩ Back to Thunk"}
        </Link>
      </div>
      <Button />
      <Loading />
      <NewsItem />
    </div>
  );
};

export default reduxSagas;
